import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.KeyStore;
import java.security.SecureRandom;
import java.security.Security;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Base64;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;
import javax.xml.crypto.dsig.XMLSignature;
import javax.xml.crypto.dsig.XMLSignatureException;
import javax.xml.crypto.dsig.XMLSignatureFactory;
import javax.xml.crypto.dsig.dom.DOMValidateContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXParseException;


public class TrustedXmlParser {
	KeyStore certKs;
	private ArrayList<Certificate> certificatesForTheXML;
	private ArrayList<String> allXml;
	TrustedXmlParser(){
		allXml = new ArrayList<String>();
	}
	public ArrayList<Certificate> openDocument()throws Exception{
		ArrayList<Certificate> allCerts=new ArrayList<Certificate>();
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		dbf.setNamespaceAware(true);
		//dbf.setValidating(false);
		//SchemaFactory sf= SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");
		//URL schema_url= new URL("http://uri.etsi.org/01903/v1.3.2/XAdES.xsd");
		//dbf.setSchema(sf.newSchema(schema_url));
		
		DocumentBuilder db = dbf.newDocumentBuilder();
		KeyStore ks=KeyStore.getInstance("JKS");
		String keyStorePassword ="Taby92";
		String generalPath = getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
		File f2=new File(generalPath);
		generalPath=f2.getParent();
		File generalJKS=Paths.get(generalPath+"//eceuropaeu.jks").toFile();
		InputStream keyStream= new FileInputStream(generalJKS);
		ks.load(keyStream,keyStorePassword.toCharArray());
		KeyManagerFactory kmf = KeyManagerFactory.getInstance("PKIX");
		kmf.init(ks, keyStorePassword.toCharArray());
		TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
		tmf.init(ks);
		SSLContext ctx = SSLContext.getInstance("TLS");
		ctx.init(kmf.getKeyManagers(),tmf.getTrustManagers(),new java.security.SecureRandom());
		SSLSocketFactory sf =ctx.getSocketFactory();
		Security.setProperty("ocsp.enable","true");
		System.setProperty("com.sun.security.enable.CRLDP", "true");
		HttpsURLConnection.setDefaultSSLSocketFactory(sf);
		
		URL test = new URL("https://ec.europa.eu/information_society/policy/esignature/trusted-list/tl-mp.xml");
		Document doc = db.parse(test.openStream());
	    Node signatureAsNode = doc.getElementsByTagNameNS(XMLSignature.XMLNS, "Signature").item(0);
	    CertificateFactory cf=CertificateFactory.getInstance("X.509");
		String PATH = getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
		File f=new File(PATH);
		PATH=f.getParent();
		byte [] decoded2 = Base64.getDecoder().decode(Files.readAllBytes(Paths.get((f.getParent()+"//Cert_for_List_of_trusted_lists_2.cer"))));
		InputStream isa=new ByteArrayInputStream(decoded2); 
		
		Certificate validationCert=cf.generateCertificate(isa);
		//((Element)signatureAsNode).setIdAttribute("Id", true);
		//System.out.println(((Element)signatureAsNode).getAttribute("Id"));
		DOMValidateContext valContext = new DOMValidateContext(validationCert.getPublicKey(),signatureAsNode);
		valContext.setIdAttributeNS((Element) signatureAsNode, null, "Id");
		valContext.setIdAttributeNS((Element) signatureAsNode.getLastChild().getFirstChild().getFirstChild(), null, "Id");
		//for(((Element)signatureAsNode).get
		XMLSignatureFactory sfactory = XMLSignatureFactory.getInstance("DOM");
		XMLSignature signature=sfactory.unmarshalXMLSignature(valContext);
	
		//System.out.println("test");
		//System.out.println(signature.validate(valContext));
		//if(true){
		if(signature.validate(valContext)){
			certificatesForTheXML = new ArrayList<Certificate>();
			NodeList digitalIds = doc.getElementsByTagName("DigitalId");
			for(int x=0;x<digitalIds.getLength();x++){
				String content=digitalIds.item(x).getChildNodes().item(1).getTextContent();
				byte [] decoded = Base64.getDecoder().decode(content.getBytes());
				InputStream is=new ByteArrayInputStream(decoded); 
				certificatesForTheXML.add(cf.generateCertificate(is));
			}
			NodeList locations = doc.getElementsByTagName("TSLLocation");
			for(int i =0;i<locations.getLength();i++){
				if(locations.item(i).getTextContent().endsWith(".xml")||locations.item(i).getTextContent().endsWith(".xtsl")){
					if(!locations.item(i).getTextContent().equals(test.toString())){
						System.out.println(locations.item(i).getTextContent());
						allXml.add(locations.item(i).getTextContent());
						try{
							allCerts.addAll(getCertificates(locations.item(i).getTextContent()));
						}catch(SAXParseException e){
							System.out.println("doofeSache");
						}
					}
				}
			}
		}else{
			throw new Exception();
		}
		return allCerts;
	}
	private ArrayList<Certificate>getCertificates(String _url) throws Exception{
		ArrayList<Certificate> certsFromSite=new ArrayList<Certificate>();
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		dbf.setNamespaceAware(true);
		dbf.setValidating(false);
		//SchemaFactory sf= SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");
		//URL schema_url= new URL("http://uri.etsi.org/01903/v1.3.2/XAdES.xsd");
		//dbf.setSchema(sf.newSchema(schema_url));
		DocumentBuilder db = dbf.newDocumentBuilder();
		URL test = new URL(_url);
		SSLContext sc=SSLContext.getInstance("SSL");
		TrustManager[] trustAllCertificates=new TrustManager[] {
				new X509TrustManager() {
            @Override
            public X509Certificate[] getAcceptedIssuers() {
                return null; // Not relevant.
            }
            @Override
            public void checkClientTrusted(X509Certificate[] certs, String authType) {
                // Do nothing. Just allow them all.
            }
            @Override
            public void checkServerTrusted(X509Certificate[] certs, String authType) {
                // Do nothing. Just allow them all.
            }
        }
		};
		sc.init(null, trustAllCertificates, new SecureRandom());
		HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
		InputStream isd=null;
		try{
			isd = test.openStream();
		}catch (Exception e){
			System.out.println("innerer Fehler");
			System.out.println(e.getMessage());
			try{
				isd=test.openStream();
			}catch(Exception e2){
				System.out.println("zweiter Versuch gescheitert");
				System.out.println(e2.getMessage());
			}
		}
		if(isd!=null) {
		Document doc = db.parse(isd);
		boolean isSave=false;
	    Node signatureAsNode = doc.getElementsByTagNameNS(XMLSignature.XMLNS, "Signature").item(0);
		for(int d=0;d<certificatesForTheXML.size();d++){
			DOMValidateContext valContext = new DOMValidateContext(certificatesForTheXML.get(d).getPublicKey(),signatureAsNode);
			valContext.setIdAttributeNS((Element) signatureAsNode, null, "Id");
			try{
				valContext.setIdAttributeNS((Element) signatureAsNode.getLastChild().getFirstChild().getFirstChild(), null, "Id");
			}catch (NullPointerException en){
				//nichts wegen den Ungarn
			}
			
			XMLSignatureFactory sfactory = XMLSignatureFactory.getInstance("DOM");
			XMLSignature signature=sfactory.unmarshalXMLSignature(valContext);
			if(doc.getFirstChild().getAttributes()!=null&&doc.getFirstChild().getAttributes().getNamedItem("Id")!=null){
				if(!doc.getFirstChild().getAttributes().getNamedItem("Id").getNodeValue().isEmpty()){
					valContext.setIdAttributeNS((Element) doc.getFirstChild(),null, "Id");
				}
			}
			if(doc.getChildNodes().getLength()>1){
				if(doc.getChildNodes().item(1).getAttributes()!=null&&doc.getChildNodes().item(1).getAttributes().getNamedItem("Id")!=null){
					if(!doc.getChildNodes().item(1).getAttributes().getNamedItem("Id").getNodeValue().isEmpty()){
						valContext.setIdAttributeNS((Element) doc.getChildNodes().item(1),null, "Id");
					}
				}
			}
			
			try{
				if(signature.validate(valContext)){
					//System.out.println("isSave");
					isSave=true;
					break;
				}
			}catch(XMLSignatureException e){
				//do nothing
				//System.out.println(e.getMessage());
			}
		}
		if(isSave){
			NodeList allTSPService= doc.getElementsByTagName("TSPService");
			for(int i=0;i<allTSPService.getLength();i++){
				NodeList childsInServiceInformation=allTSPService.item(i).getChildNodes().item(1).getChildNodes();
				boolean isWebsiteAuth=false;
				boolean isGranted=false;
				Certificate cert=null;
				for(int j=0;j<childsInServiceInformation.getLength();j++){
					if(childsInServiceInformation.item(j).getNodeName().equals("ServiceStatus")){
						if(childsInServiceInformation.item(j).getTextContent().endsWith("granted")){
							isGranted=true;
						}
					}else if(childsInServiceInformation.item(j).getNodeName().equals("ServiceInformationExtensions")){
						for(int z=0;z<childsInServiceInformation.item(j).getChildNodes().getLength();z++){
							if(childsInServiceInformation.item(j).getChildNodes().item(z).getNodeName().equals("Extension")){
								for(int z2=0;z2<childsInServiceInformation.item(j).getChildNodes().item(z).getChildNodes().getLength();z2++){
									if(childsInServiceInformation.item(j).getChildNodes().item(z).getChildNodes().item(z2).getNodeName().equals("AdditionalServiceInformation")){
										for(int z3=0;z3<childsInServiceInformation.item(j).getChildNodes().item(z).getChildNodes().item(z2).getChildNodes().getLength();z3++){
											if(childsInServiceInformation.item(j).getChildNodes().item(z).getChildNodes().item(z2).getChildNodes().item(z3).getNodeName().equals("URI")){
												if(childsInServiceInformation.item(j).getChildNodes().item(z).getChildNodes().item(z2).getChildNodes().item(z3).getTextContent().contains("ForWebSiteAuthentication")){
													isWebsiteAuth=true;	
												}
											}
										}
									}
								}
							}
						}
					}
				}

				if(isGranted&&isWebsiteAuth){
					for(int j=0;j<childsInServiceInformation.getLength();j++){
						if(childsInServiceInformation.item(j).getNodeName().equals("ServiceDigitalIdentity")){
							for(int h=0;h<childsInServiceInformation.item(j).getChildNodes().getLength();h++){
								if(childsInServiceInformation.item(j).getChildNodes().item(h).getNodeName().equals("DigitalId")){
									CertificateFactory cf=CertificateFactory.getInstance("X.509");
									String ausgelesenes_cert=childsInServiceInformation.item(j).getChildNodes().item(h).getChildNodes().item(1).getTextContent();
									ausgelesenes_cert=ausgelesenes_cert.replaceAll("\n", "").replaceAll("\r", "");
									byte [] decoded = Base64.getDecoder().decode(ausgelesenes_cert);
									InputStream is=new ByteArrayInputStream(decoded); 
									cert=cf.generateCertificate(is);
									break;
								}
							}
						}
					}
				}				
				if(cert!=null){
					certsFromSite.add(cert);
					System.out.println("cert gefunden");
				}
			}
			
		}
		}
		
		
		return certsFromSite;
		
	}
	
	static public void main(String[] args){
		TrustedXmlParser np=new TrustedXmlParser();
		try {
			np.openDocument();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
}
