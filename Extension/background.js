chrome.tabs.onActivated.addListener( function (tabId, changeInfo, tab) {
	setEvaluation();
    connect();
})
chrome.tabs.onUpdated.addListener( function (tabId, changeInfo, tab) {
	setEvaluation();
  if (changeInfo.status == 'complete' && tab.active) {
    connect();
  }
})
function setEvaluation(){
		chrome.browserAction.setIcon({path:"european_union_round_icon_64.png"});
		chrome.browserAction.setPopup({popup:"popup_evaluation.html"});
		chrome.browserAction.setTitle({title:"waiting for connection evaluation"});
}
function connect() {
	var hostName = "eu.native.messaging.app";
	getCurrentTabUrl(function (url){
	chrome.runtime.sendNativeMessage(hostName,{"text":url},verify);
	});

}
	

// sleep time expects milliseconds
function sleep (time) {
  return new Promise((resolve) => setTimeout(resolve, time));
}

function verify(message){
	if(JSON.stringify(message)=="true"){
		chrome.browserAction.setIcon({path:"EU_trust_mark_logo_positive_500px_reduziert.jpg"});
		chrome.browserAction.setPopup({popup:"popup_success.html"});
		chrome.browserAction.setTitle({title:"connection secure"});
	}else if(JSON.stringify(message)=="false"){
		chrome.browserAction.setIcon({path:"EU_trust_mark_logo_colourless.jpg"});
		chrome.browserAction.setPopup({popup:"popup_fail.html"});
		chrome.browserAction.setTitle({title:"connection not secure"});
	}else{
		sleep(30000);
		connect();
	}
};

function getCurrentTabUrl(callback) {  // Query filter to be passed to chrome.tabs.query - see
  // https://developer.chrome.com/extensions/tabs#method-query
  var queryInfo = {
    active: true,
    currentWindow: true
  };

  chrome.tabs.query(queryInfo, function(tabs) {
    // chrome.tabs.query invokes the callback with a list of tabs that match the
    // query. When the popup is opened, there is certainly a window and at least
    // one tab, so we can safely assume that |tabs| is a non-empty array.
    // A window can only have one active tab at a time, so the array consists of
    // exactly one tab.
    var tab = tabs[0];

    // A tab is a plain object that provides information about the tab.
    // See https://developer.chrome.com/extensions/tabs#type-Tab
    var url = tab.url;

    // tab.url is only available if the "activeTab" permission is declared.
    // If you want to see the URL of other tabs (e.g. after removing active:true
    // from |queryInfo|), then the "tabs" permission is required to see their
    // "url" properties.
    console.assert(typeof url == 'string', 'tab.url should be a string');

    callback(url);
  });

  // Most methods of the Chrome extension APIs are asynchronous. This means that
  // you CANNOT do something like this:
  //
  // var url;
  // chrome.tabs.query(queryInfo, function(tabs) {
  //   url = tabs[0].url;
  // });
  // alert(url); // Shows "undefined", because chrome.tabs.query is async.
}